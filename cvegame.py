from flask import Flask,Response
app = Flask(__name__)

import json,random,os,sys

cve_dir = 'cves'

def selectCVEs(cve_dir,n):
    """Choose n CVEs from the given directory"""
    files = sorted(os.listdir(cve_dir))
    nb_of_cves = len(files)
    distance = int(nb_of_cves / n)
    startpos = random.randint(0,distance)

    #print('D = ' + str(distance))
    #print('S = ' + str(startpos))

    cves = []
    for i in range(n):
        #print(i)
        #print(os.path.join(cve_dir,files[startpos + i*distance]))
        filename = os.path.join(cve_dir,files[startpos + i*distance])
        with open(filename) as f:
            cves.append(json.load(f))

    random.shuffle(cves)
    return cves

@app.route("/")
def sendIndexPage():
    return app.send_static_file('cvegame.html')

@app.route("/random_cves")
def sendCVEs():
    cves = selectCVEs(cve_dir,5)
    return Response(json.dumps(cves), mimetype='text/json')

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
