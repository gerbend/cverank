var CVEs;

// sort array of objects by value of specific key in object
function sortByKey(array, key) {
  return array.sort(function (a, b) {
    var x = a[key]; var y = b[key];
    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
  });
}

function startExercise() {
  $.getJSON('/random_cves', function (data) { renderCVEs(data); });
}

function renderCVEs(cves) {
  CVEs = cves;
  // 20171001 Kurt: als je toch jquery gebruikt: kan dit veel korter:
  // idem voor alle andere document.getElementById();
  // var cvesUL = document.getElementById('sortable');
  var cvesUL = $('#sortable');

  var html = '';
  for (var i = 0; i < cves.length; i++) {
    var id = cves[i]['ID'];
    var desc = cves[i]['description'];
    var score = cves[i]['basescore']; // 20171001 Kurt: je doet niets met deze variabele?
    html += '<li><div class="cve"><div class="cve_id" style="display:none">' + id + '</div>';
    html += '<div class="cve_header"><div class="cve_header_arrows">&#x25B2;<br/>&#x25BC;</div><div class="cve_header_letter">' + String.fromCharCode(65 + i) + '</div></div>';
    html += '<div id="' + id + '_desc" class="cve_desc"><p class="cve_desc">' + desc + '</p></div>';
    html += '<div class="cve_score" id="' + id + '_score">0.0</div></div></li>';
  }
  html += '</div></li>';
  cvesUL.innerHTML = html;
}

function getColor(score) {
  // 20171001 Kurt: ook mogelijk om decimale waarde mee te geven
  return 'rgb(256, ' + Math.round(25.6 * (10 - score)) + ', 0)';
  /*
  var greenhex = green.toString(16);
  greenhex = (greenhex.length < 2 ? '0' : '') + greenhex;
  return '#ff' + greenhex + '00';
  */
}

function getActualRanking() {
  var rankedCVEs = CVEs.slice(0);
  sortByKey(rankedCVEs, 'basescore');
  var result = [];
  for (var i = 0; i < rankedCVEs.length; i++) {
    result.push(rankedCVEs[i]['ID']);
  }
  return result;
}

function getSubmittedRanking() {
  var l = document.getElementById('sortable');
  var list = l.getElementsByClassName("cve_id");
  var result = [];
  for (var i = 0; i < list.length; i++) {
    result.push(list[i].innerHTML);
  }
  return result;
}

function compareRankings(r1, r2) {
  // check lengths are equal, contains same elements?
  var score = 0;
  for (var i = 0; i < r1.length; i++) {
    score += Math.abs(i - r2.indexOf(r1[i]));
  }
  score = 10 * (1 - (2 * score / Math.pow(r1.length, 2)));
  return score;
}

function checkRanking() {
  var submittedRanking = getSubmittedRanking();
  var actualRanking = getActualRanking();
  for (var i = 0; i < CVEs.length; i++) {
    var score_el = document.getElementById(CVEs[i]['ID'] + '_score');
    var desc_el = document.getElementById(CVEs[i]['ID'] + '_desc');
    score_el.innerHTML = CVEs[i]['basescore'];
    score_el.style.visibility = 'visible';
    var color = getColor(CVEs[i]['basescore']);
    score_el.style.backgroundColor = color;
    desc_el.style.backgroundColor = color;
    var desc_p = desc_el.children[0];
    desc_p.innerHTML = desc_p.innerHTML + ' [<a href="https://nvd.nist.gov/vuln/detail/' + CVEs[i]['ID'] + '" target="_blank">' + CVEs[i]['ID'] + '</a>]';
  }

  var btn = document.getElementById('button');
  var resultDiv = document.getElementById('result');
  resultDiv.innerHTML = 'Your score: ' + 10 * Math.round(compareRankings(submittedRanking, actualRanking)) + ' %';
  resultDiv.innerHTML.visibility = 'visible';
  btn.onclick = resetExercise;
  btn.value = 'Try again';


}

function resetExercise() {
  //GEWOON PAGE REFRESH MISSCHIEN SIMPELER?
  // Kurt: ja maar niet zo proper ;)
  var btn = document.getElementById('button');
  var resultDiv = document.getElementById('result');
  resultDiv.innerHTML = '';
  resultDiv.innerHTML.visibility = 'hidden';
  btn.onclick = checkRanking;
  btn.value = 'Check';
  startExercise(cves);
}
